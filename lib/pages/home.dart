import 'package:flutter/material.dart';
import 'package:flutter_dashboard/widgets/gauge_chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  double sliderValue = 0;

  @override
  Widget build(BuildContext context) {
    List<charts.Series<GaugeSegment, String>> speedDisplay = [
      charts.Series<GaugeSegment, String>(
        id: 'Segments',
        domainFn: (GaugeSegment segment, _) => segment.segment,
        measureFn: (GaugeSegment segment, _) => segment.size,
        colorFn: (GaugeSegment segment, _) => segment.color,
        data: [
          GaugeSegment('Active', sliderValue.toInt(),
              charts.Color(r: 255, g: 193, b: 7)),
          GaugeSegment('Empty', 180 - sliderValue.toInt(),
              charts.Color(r: 255, g: 255, b: 255)),
        ],
      )
    ];

    return Scaffold(
      drawer: Drawer(),
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.amber,
        title: Text('Home'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
              height: 200.0,
              child: GaugeChart(
                speedDisplay,
                sliderValue.toInt(),
                animate: false,
              )),
          Container(
            child: Slider(
              activeColor: Colors.amber,
              inactiveColor: Colors.amber.withOpacity(.2),
              min: 0,
              max: 180,
              divisions: 180,
              onChanged: (value) {
                setState(() {
                  this.sliderValue = value;
                });
              },
              value: sliderValue,
            ),
          )
        ],
      ),
    );
  }
}
