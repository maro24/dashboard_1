import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'dart:math';

class GaugeChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;
  final int value;

  GaugeChart(this.seriesList, this.value, {this.animate});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        charts.PieChart(seriesList,
            animate: animate,
            defaultRenderer: charts.ArcRendererConfig(
                arcWidth: 30, startAngle: 4 / 5 * pi, arcLength: 7 / 5 * pi)),
        Center(
          child: Container(
            child: Text('$value',
                style: TextStyle(color: Colors.amber, fontSize: 28.0)),
          ),
        )
      ],
    );
  }
}

class GaugeSegment {
  final String segment;
  final int size;
  final charts.Color color;

  GaugeSegment(this.segment, this.size, this.color);
}
